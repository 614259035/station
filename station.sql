-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2020 at 05:46 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `station`
--

-- --------------------------------------------------------

--
-- Table structure for table `break_point`
--

CREATE TABLE `break_point` (
  `id` int(11) NOT NULL,
  `b_to` time NOT NULL,
  `b_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `break_point`
--

INSERT INTO `break_point` (`id`, `b_to`, `b_out`) VALUES
(6, '05:53:00', '05:54:00'),
(7, '07:39:00', '07:40:00'),
(8, '08:22:00', '08:23:00'),
(9, '09:38:00', '09:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE `destination` (
  `id` int(11) NOT NULL,
  `d_station` varchar(50) NOT NULL,
  `d_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`id`, `d_station`, `d_time`) VALUES
(6, 'กรุงเทพ', '14:15:00'),
(7, 'นครราชสีมา', '09:50:00'),
(8, 'กรุงเทพ', '14:55:00'),
(9, 'นครราชสีมา', '11:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `origin`
--

CREATE TABLE `origin` (
  `id` int(11) NOT NULL,
  `o_station` varchar(50) NOT NULL,
  `o_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `origin`
--

INSERT INTO `origin` (`id`, `o_station`, `o_time`) VALUES
(6, 'สุรินทร์', '05:20:00'),
(7, 'สำโรงทาบ', '05:50:00'),
(8, 'อุบลราชธานี', '05:40:00'),
(9, 'อุบลราชธานี', '06:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `procession`
--

CREATE TABLE `procession` (
  `id` int(11) NOT NULL,
  `t_id` varchar(5) NOT NULL,
  `note` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `procession`
--

INSERT INTO `procession` (`id`, `t_id`, `note`) VALUES
(6, '234', 'รถธรรมดา'),
(7, '424', 'รถดีเซลราง'),
(8, '72', 'รถด่วนดีเซลราง'),
(9, '428', 'รถดีเซลราง');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `break_point`
--
ALTER TABLE `break_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `origin`
--
ALTER TABLE `origin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procession`
--
ALTER TABLE `procession`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `break_point`
--
ALTER TABLE `break_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `destination`
--
ALTER TABLE `destination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `origin`
--
ALTER TABLE `origin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `procession`
--
ALTER TABLE `procession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `break_point`
--
ALTER TABLE `break_point`
  ADD CONSTRAINT `break_point_ibfk_1` FOREIGN KEY (`id`) REFERENCES `procession` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `destination`
--
ALTER TABLE `destination`
  ADD CONSTRAINT `destination_ibfk_1` FOREIGN KEY (`id`) REFERENCES `procession` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `origin`
--
ALTER TABLE `origin`
  ADD CONSTRAINT `origin_ibfk_1` FOREIGN KEY (`id`) REFERENCES `procession` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
