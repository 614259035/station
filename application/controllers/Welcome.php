<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model', 'model');
    }

	public function index()
	{
		$this->load->view('home');
	}

	public function insert()
    {

        $data = array(
            't_id' => $this->input->post("t_id"),
            'note' => $this->input->post("note")
       
        );

        $data2 = array(
            'o_station' => $this->input->post("o_station"),
            'o_time' => $this->input->post("o_time")
            
        );
        $data3 = array(
            'b_to' => $this->input->post("b_to"),
            'b_out' => $this->input->post("b_out")
        );
        $data4 = array(
            'd_station' => $this->input->post("d_station"),
            'd_time' => $this->input->post("d_time")

        );
        $this->model->procession($data);
        $this->model->origin($data2);
        $this->model->break_point($data3);
        $this->model->destination($data4);
	}
	
	public function show()
    {
        $data['query'] = $this->model->show();
        $this->load->view('show', $data);
    }




	
}
