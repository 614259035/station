<?php defined('BASEPATH') or exit('No direct script access allowed');

class Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    function procession($data)
    {
        $this->db->insert('procession', $data);
    }

    function origin($data)
    {
        $this->db->insert('origin', $data);
    }

    function break_point($data)
    {
        $this->db->insert('break_point', $data);
    }

    function destination($data)
    {
        $this->db->insert('destination', $data);
    }

    function show()
    {

        $this->db->select('*');
        $this->db->from('procession');
        $this->db->join('origin', 'origin.id = procession.id', 'left');
        $this->db->join('break_point', 'break_point.id = procession.id', 'left');
        $this->db->join('destination', 'destination.id = procession.id', 'left');
    
        $this->db->where('origin.o_station','อุบลราชธานี');
        $this->db->where('destination.d_station','นครราชสีมา');
        $query = $this->db->get();
        return $query->result();
    }


}
