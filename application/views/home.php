<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="app">
   <div class="wrapper">
      <div class="card">

    <form action="<?php echo site_url('Welcome/insert'); ?>" method="post">

         <header class="card__header">
            <div class="card__content">
               <h1 class="card__head">Add Station</h1>
               <p class="card__text">Choose a destination station, rest point Set the time of departure of the station</p>
            </div>


            <div class="card__form">
               <input class="card__input" name="t_id" type="text" placeholder="ขบวน">

               <select class="card__select" name="note" id="ct">
                  <option  value="รถธรรมดา">รถธรรมดา</option>
                   <option value="รถดีเซลราง">รถดีเซลราง</option>
                    <option value="รถด่วนดีเซลราง">รถด่วนดีเซลราง</option>
               </select>
        
           </div>
         </header>

         <main class="main">
            <div class="main-grid">
               <div class="post">
                  <header class="post__hd">
                      <span class="post__icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26">
                           <g fill="none" fill-rule="evenodd">
                              <path d="M-4-4h34v34H-4z" />
                              <path stroke="#2ECF9A" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.125" d="M18.667 4.5h4.25c.782 0 1.416.634 1.416 1.417V21.5a2.833 2.833 0 11-5.666 0V3.083c0-.782-.635-1.416-1.417-1.416H3.083c-.782 0-1.416.634-1.416 1.416v17a4.25 4.25 0 004.25 4.25H21.5m-14.167-17H13M7.333 13H13m-5.667 5.667H13" />
                           </g>
                        </svg>
                     </span>
                     <h3 class="post__title">Station <br>
                        ต้นทาง</h3>
                  </header><br>
                   <p class="post__text">สถานีต้นทาง</p>
                <input class="card__input" name="o_station" type="text" placeholder="สถานี">
                 <p class="post__text">เวลาออก</p>
                <input class="date__input" name="o_time" type="time" placeholder="เวลาออก">
               </div>

               <div class="post">
                  <header class="post__hd">
                     <span class="post__icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26">
                           <g fill="none" fill-rule="evenodd">
                              <path d="M-4-4h34v34H-4z" />
                              <path stroke="#2ECF9A" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.125" d="M18.667 4.5h4.25c.782 0 1.416.634 1.416 1.417V21.5a2.833 2.833 0 11-5.666 0V3.083c0-.782-.635-1.416-1.417-1.416H3.083c-.782 0-1.416.634-1.416 1.416v17a4.25 4.25 0 004.25 4.25H21.5m-14.167-17H13M7.333 13H13m-5.667 5.667H13" />
                           </g>
                        </svg>
                     </span>
                     <h3 class="post__title">Station <br>
                        จุดพัก</h3>
                  </header><br>
                <p class="post__text">เวลาถึง</p>
                <input class="date__input" name="b_to" type="time" placeholder="เวลาถึง">
                 <p class="post__text">เวลาออก</p>
                <input class="date__input" name="b_out" type="time" placeholder="เวลาออก">
               </div>



               <div class="post">
                  <header class="post__hd">
                     <span class="post__icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="27">
                           <g transform="translate(-3 -3)" fill="none" fill-rule="evenodd">
                              <path d="M0 0h34v34H0z" />
                              <rect width="25.5" height="18.417" x="4.25" y="9.917" stroke="#2ECF9A" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.125" rx="2.833" />
                              <path stroke="#2ECF9A" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.125" d="M11.333 9.917V7.083a2.833 2.833 0 012.834-2.833h5.666a2.833 2.833 0 012.834 2.833v2.834M17 17v.014M4.25 18.417a28.333 28.333 0 0025.5 0" />
                           </g>
                        </svg>
                     </span>
                     <h3 class="post__title">Station <br>
                        ปลายทาง</h3>
                  </header><br>
                   <p class="post__text">สถานีปลายทาง</p>
                   <input class="card__input" name="d_station" type="text" placeholder="สถานี">
                    <p class="post__text">ถึงเวลา</p>
                    <input class="date__input" name="d_time" type="time" placeholder="เวลาถึง">
               </div>
            </div>
                    <input class="card__btn" value="บันทึก" type="submit">
          </form>
         </main>
    
          
      </div>
   </div>
    <a href="<?php echo site_url('Welcome/show'); ?>"><button class="show__btn">ดูตาราง</button></a>
</div>

<style>
@import url("https://fonts.googleapis.com/css2?family=Muli:wght@400;600;700;800&display=swap");

root {
   --clr-head: #2f3144;
   --clr-body: #2f3144;
   --clr-main: #2ecf9a;
   --fs-h1: 2.8rem;
   --fs-h3: 1.8rem;
   --fw-r: 400;
   --fw-sb: 600;
   --fw-b: 700;
   --fw-xb: 800;
   box-sizing: border-box;
}
*,
*::before,
*::after {
   margin: 0;
   padding: 0;
   box-sizing: inherit;
}
html,
body {
   width: 100%;
   min-height: 100vh;
   font-size: 62.5%;
}
body {
   font-family: "Muli", sans-serif;
   font-size: 1.6rem;
   line-height: 1.3;
   background-color: #edf4f3;
   color: #2f3144;
   display: flex;
   justify-content: center;
   align-items: center;
}

.app {
   max-width: 1131px;
   width: 100%;
   margin: 0 auto;
   padding: 2em 1em;
   border-radius: 10px;
   background-color: #fff;
   box-shadow: 0 15px 9px -10px rgba(black, 0.1);
}

.wrapper {
   max-width: 1031px;
   width: 100%;
   margin: auto;
   padding: 0 0.5em;
}

.card {
   display: flex;
   flex-direction: column;
   align-items: center;
}

.card__header {
   width: 90%;
   max-width: 100%;
}

.card__head {
   margin: 0 0 0.5em 0;
   font-size: 2.5rem;
}

.card__content {
   text-align: center;
   margin-bottom: 2em;
}
.card__title {
   font-size: 1.8rem;
   margin-bottom: 0.5em;
}

.card__text {
   color: rgba(black, 0.5);
}

.card__form {
   display: flex;
   flex-direction: column;
}

.card__select {
   margin-bottom: 1em;
   display: block;
   line-height: 1.3;
   max-width: 100%;
   // padding: 0.6em 1.4em 0.5em 0.8em;
   padding: 1em 1.4em 0.8em;
   appearance: none;
   border: 1px solid #cdcdcd;
   border-radius: 6px;
   background-image: url("https://assets.codepen.io/2709552/select-arrow.svg");
   background-repeat: no-repeat;
   background-position: right 0.7em top 50%, 0 0;
   background-size: 0.65em auto, 100%;
}

.card__select:focus {
   border-color: hsla(168, 100%, 42%, 0.6);
   outline: none;
}

.card__input {
   margin-bottom: 1em;
   display: block;
   line-height: 1.3;
   max-width: 100%;
  margin-right:10px;
   padding: 1em 1.4em 0.8em;
   appearance: none;
   border: 1px solid #cdcdcd;
   border-radius: 6px;
}
.date__input {
margin-bottom: 1em;
   display: block;
   line-height: 1.3;
   max-width: 100%;
  margin-right:10px;
   padding: 1em 1.4em 0.8em 9em;
   appearance: none;
   border: 1px solid #cdcdcd;
   border-radius: 6px;
}



.card__btn {
   display: inline-flex;
   justify-content: center;
   font-size: 1.8rem;
   text-align: center;
   border-radius: 6px;
   background-color: #2ecf9a;
   color: white;
   text-decoration: none;
   padding: 1em 2em;
   margin: 1em 0;
}
.card__btn:hover{
    background-color: #48F5BC;
}
.show__btn {
   display: inline-flex;
   justify-content: center;
   font-size: 1.8rem;
   text-align: center;
   border-radius: 6px;
   background-color: #9DBDFE;
   color: white;
   text-decoration: none;
   padding: 1em 2em;
   margin: 1em 2em 0;
    float: right;
}
.show__btn:hover{
   background-color:#BBD1FF ;  
}

a{
    text-decoration: none;
}

.main {
   padding: 1em 0;
   width: 90%;
   max-width: 100%;
}

.main-grid {
   display: grid;
   grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
   gap: 2rem;
}

.post__hd {
   display: flex;
   align-items: center;
   margin-bottom: 0.5em;
}

.post__title {
   font-size: 1.7rem;
   margin-left: 0.8em;
}

.post__text {
   color: rgba(black, 0.5);
   font-size: 1.6rem;
}

@media (min-width: 900px) {
   .card__header {
      width: 100%;
      max-width: 100%;
      display: flex;
      align-items: center;
      justify-content: space-between;
   }

   .card__head {
      font-size: 2rem;
   }
   .card__form {
      display: flex;
      flex-direction: row;
   }
   .card__content {
      text-align: left;
      margin-bottom: 2em;
      flex: 0 0 30%;
   }

   .card__select {
      width: 229px;
      margin-right: 1.5rem;
   }

   .card__btn {
      margin: 0;
      padding: 0;
      font-size: 1.6rem;
      width: 155px;
      height: 53px;
      justify-content: center;
      align-items: center;
      box-shadow: 0 9px 10px -7px rgba(0, 213, 172, 0.6);
   }

   .main {
      width: 100%;
      background-color: rgba(237, 244, 242, 1);
      padding: 1em 1em;
      border-radius: 6px;
   }
}


</style>