<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <a href="<?php echo site_url('Welcome'); ?>"><button class="show__btn">Home</button></a>
  </div>
  <div class="container">
    <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ขบวน</th>
      <th scope="col">สถานีต้นทาง</th>
      <th scope="col">เวลาออก</th>
      <th scope="col">ถึงจุดพักเวลา</th>
      <th scope="col">ออกจุดพักเวลา</th>
      <th scope="col">สถานีปลายทาง</th>
      <th scope="col">ถึงเวลา</th>
      <th scope="col">หมายเหตุ</th>
    </tr>
  </thead>
<?php foreach($query as $list) {?>
  <tbody>
    <tr>
      <th scope="row"><?php echo $list->t_id;?></th>
      <th><?php echo $list->o_station;?></th>
      <th ><?php echo $list->o_time;?></th>
      <th><?php echo $list->b_to;?></th>
      <th><?php echo $list->b_out;?></th>
      <th><?php echo $list->d_station;?></th>
      <th><?php echo $list->d_time;?></th>
      <th><?php echo $list->note;?></th>
      
    </tr>
  
  </tbody>
<?php } ?>
</table>
</div>
</body>
</html>

<style>
  .show__btn {
   display: inline-flex;
   justify-content: center;
   font-size: 1.1rem;
   text-align: center;
   border-radius: 6px;
   background-color: #9DBDFE;
   color: white;
   text-decoration: none;

   margin: 5px;
    float: right;
}
.show__btn:hover{
   background-color: #9AFEEF;
}
</style>

